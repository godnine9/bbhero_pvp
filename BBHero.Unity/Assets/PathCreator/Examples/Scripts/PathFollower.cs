﻿using UnityEngine;

namespace PathCreation.Examples
{
    // Moves along a path at constant speed.
    // Depending on the end of path instruction, will either loop, reverse, or stop at the end of the path.
    public class PathFollower : MonoBehaviour
    {
        public PathCreator pathCreator;
        public EndOfPathInstruction endOfPathInstruction;
        public float speed = 5;
        public float distanceTravelled;
        public bool Throwed;

        public void Play() {
            if (pathCreator != null)
            {
                Throwed = true;
            }
        }

        public void Pause()
        {
            if (pathCreator != null)
            {
                Throwed = true;
            }
        }

        public void Stop()
        {
            if (pathCreator != null)
            {
                Throwed = true;
            }
        }

        void Update()
        {
            if (pathCreator != null && Throwed)
            {
                distanceTravelled += speed * Time.deltaTime;
                transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
                transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);

                if(distanceTravelled / pathCreator.path.length >= 1)
                {
                    distanceTravelled = 0;
                    Throwed = false;
                }
            }
        }

    }
}