using PathCreation;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace Ninth.BBHero
{
    public enum Step
    {
        BeforePitch,
        Pitched,
        Hit,
        StepGap,
        GameOver
    }

    public class NewGameManager : MonoBehaviourPunCallbacks
    {
        static public NewGameManager Instance;
        public PhotonView PV;
        public Text TeamName;
        public Text TeamScores;
        public Text Status;
        #region Public Fields
        public Camera MainCamera;
        public Animator Batter;
        public Animator Pitcher;
        public GameObject batterControler;
        public GameObject pitcherControler;
        public GameObject HomeRunHitParticel;
        public GameObject NormalHitParticel;
        #endregion
        public GameObject HitResult;
        public Text HitResulttext;

        #region Pitcher Fields
        public RectTransform Target;
        public Slider PowerSlider;
        public float senstive = 200;


        private int PitchPath = -1;
        private float Power = 0;
        #endregion

        #region GameResult
        public GameObject GameResult;
        public TMP_Text ResultMasterName;
        public TMP_Text ResultCustomName;
        public TMP_Text ResultMasterScore;
        public TMP_Text ResultCustomScore;
        public Button LeavRoom;
        #endregion
        public RectTransform CanvasRect;
        public RectTransform BallThrowedAim;
        public Vector3 OrigenPos;
        public Transform Ball;
        public PathCreator StraightPath;
        public PathCreator CurvetPath;
        public PathCreator KnucklePath;

        public bool IsPitcher = false;
        public bool IsThrowed = false;
        public bool IsSwinged = false;
        public bool IsMasterTeam = false;
        public int GameCounter;
        private float XRate = 0.00216f;
        private float YRate = 0.00216f;

        public PathCreator pathCreator;
        private float CurSpeed = 10;
        private float distanceTravelled;

        public Step StepIndex;
        private float StepUpdate;

        private int Strike;
        private int BadBall;
        private int Out;
        private string MasterName;
        private string CustomName;
        private int MasterScore;
        private int CustomScore;

        void Start()
        {
            Instance = this;

            // in case we started this demo with the wrong scene being active, simply load the menu scene
            if (!PhotonNetwork.IsConnected)
            {
                SceneManager.LoadScene("Start");
                return;
            }

            batterControler.gameObject.SetActive(!PhotonNetwork.LocalPlayer.IsMasterClient);
            pitcherControler.gameObject.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);

            BallThrowedAim.gameObject.SetActive(false);
            var Playerlist = PhotonNetwork.CurrentRoom.Players;
            TeamName.text = "";
            foreach (var player in Playerlist.Values)
            {
                if (player.IsMasterClient)
                {
                    MasterName = player.NickName;
                }
                else
                {
                    CustomName = player.NickName;
                }
            }

            Power = 0;
            StepIndex = Step.BeforePitch;
            PowerSlider.gameObject.SetActive(false);
            HitResult.gameObject.SetActive(false);
            HomeRunHitParticel.gameObject.SetActive(false);
            NormalHitParticel.gameObject.SetActive(false);
            PowerSlider.value = Power;
            Strike = 0;
            BadBall = 0;
            Out = 0;
            MasterScore = 0;
            CustomScore = 0;
            TeamName.text = $"{CustomName} : {MasterName}";
            TeamScores.text = $"{CustomScore}  :  {MasterScore}";
            Status.text = $"S: {Strike}  B: {BadBall}   Out:  {Out}";
            IsPitcher = PhotonNetwork.LocalPlayer.IsMasterClient;
            IsMasterTeam = false;
            GameCounter = 0;
            NinthGames.AudioManager.play_music("Audio/BGM/Game", 0.5f);
        }
        [PunRPC]
        public void PitcherThrow(int PathIndex, float Power, float Speed, Vector3 offset)
        {
            if (PhotonNetwork.LocalPlayer.IsLocal)
            {
                StepIndex = Step.Pitched;
                IsThrowed = false;
                CurSpeed = 0;
                Pitcher.Play("Pitch");
                StepUpdate = 0;
                switch (PathIndex)
                {
                    case 0:
                        pathCreator = StraightPath;
                        CurSpeed = 20 + Power * 10;
                        break;
                    case 1:
                        pathCreator = CurvetPath;
                        CurSpeed = 15 + Power * 10;
                        break;
                    case 2:
                        pathCreator = KnucklePath;
                        CurSpeed = 15 + Power * 5;
                        break;
                }
                pathCreator.gameObject.transform.position = offset;
            }
        }

        [PunRPC]
        public void Swing()
        {
            Batter.Play("Hit");
            NinthGames.AudioManager.PlaySound("batter_swing_strong");
            StepUpdate = 0;
            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                if (StepIndex == Step.Pitched && !IsSwinged)
                {
                    IsSwinged = true;
                    if (distanceTravelled / pathCreator.path.length < 0.8f)
                    {
                        //過快
                        Strike++;
                        if (Strike >= 3)
                        {
                            Out += 1;
                            Strike = 0;
                            BadBall = 0;
                        }
                        photonView.RPC("ShowResult", RpcTarget.All, "哈哈沒打到", Strike, BadBall, Out, MasterScore, CustomScore);
                    }
                    else if (distanceTravelled / pathCreator.path.length >= 0.93f && distanceTravelled / pathCreator.path.length < 0.97f)
                    {
                        float HitResult = UnityEngine.Random.Range(0, 100);
                        Vector3 Force = MainCamera.transform.up * 5 + MainCamera.transform.right * UnityEngine.Random.Range(-20f, 20f);
                        photonView.RPC("ShowNormal", RpcTarget.All, Force);
                        if (HitResult > 50)//接殺
                        {
                            Out += 1;
                            if (Out > 3) { Out = 3; }
                            photonView.RPC("ShowResult", RpcTarget.All, "接殺", Strike, BadBall, Out, MasterScore, CustomScore);
                        }
                        else // 界外
                        {
                            Strike += 1;
                            if (Strike > 2) { Strike = 2; }
                            photonView.RPC("ShowResult", RpcTarget.All, "Foul", Strike, BadBall, Out, MasterScore, CustomScore);
                        }

                        Ball.gameObject.SetActive(true);
                        StepIndex = Step.Hit;
                    }
                    else if (distanceTravelled / pathCreator.path.length >= 0.97f && distanceTravelled / pathCreator.path.length < 1.0f)
                    {
                        if (IsMasterTeam)
                        {
                            MasterScore += 1;
                        }
                        else
                        {
                            CustomScore += 1;
                        }
                        Strike = 0;
                        BadBall = 0;
                        Vector3 Force = MainCamera.transform.forward * 5 + MainCamera.transform.up * 5 + MainCamera.transform.right * UnityEngine.Random.Range(-5f, 5f);
                        photonView.RPC("ShowHomeRun", RpcTarget.All, Force);
                        photonView.RPC("ShowResult", RpcTarget.All, "HomeRun", Strike, BadBall, Out, MasterScore, CustomScore);

                        Ball.gameObject.SetActive(true);
                        StepIndex = Step.Hit;
                    }
                }
            }
        }

        [PunRPC]
        public void CatchBall()
        {
            pathCreator.gameObject.transform.position = OrigenPos;
            Ball.gameObject.SetActive(false);
            NinthGames.AudioManager.PlaySound("catcher_mitt3");
        }

        [PunRPC]
        public void ShowHomeRun(Vector3 Force)
        {
            NinthGames.AudioManager.PlaySound("batting_Specal hit1");
            HomeRunHitParticel.transform.position = Ball.transform.position;
            HomeRunHitParticel.gameObject.SetActive(true);
            Ball.GetComponent<Rigidbody>().AddForce(Force, ForceMode.Impulse);
        }

        [PunRPC]
        public void ShowNormal(Vector3 Force)
        {
            NinthGames.AudioManager.PlaySound("batting_hit");
            NormalHitParticel.transform.position = Ball.transform.position;
            NormalHitParticel.gameObject.SetActive(true);
            Ball.GetComponent<Rigidbody>().AddForce(Force, ForceMode.Impulse);
        }

        [PunRPC]
        public void ShowResult(string Result,int iStrike,int iBadBall,int iOut,int iMasterScore, int iCustomScore)
        {
            Strike = iStrike;
            BadBall = iBadBall;
            Out = iOut;
            MasterScore = iMasterScore;
            CustomScore = iCustomScore;
            HitResult.SetActive(true);
            HitResulttext.text = Result;
            StepIndex = Step.StepGap;
            TeamScores.text = $"{MasterScore}  :  {CustomScore}";
            Status.text = $"S: {Strike}  B: {BadBall}   Out:  {Out}";
            if (Out >=3)
            {
                GameCounter++;
                if (GameCounter >= 2)
                {
                    photonView.RPC("GameOver", RpcTarget.All, null);
                }
                else
                {
                    Out = 0;
                    Strike = 0;
                    BadBall = 0;
                    IsMasterTeam = !IsMasterTeam;
                    IsPitcher = !IsPitcher;
                    batterControler.gameObject.SetActive(!IsPitcher);
                    pitcherControler.gameObject.SetActive(IsPitcher);
                    HitResulttext.text = $"攻守交換";
                    Status.text = $"S: {Strike}  B: {BadBall}   Out:  {Out}";
                }
            }
        }

        [PunRPC]
        public void GameOver()
        {
            GameResult.gameObject.SetActive (true);
            ResultMasterScore.text = MasterScore.ToString();
            ResultCustomScore.text = CustomScore.ToString();
            ResultMasterName.text = MasterName;
            ResultCustomName.text = CustomName;
            StepIndex = Step.GameOver;
            batterControler.gameObject.SetActive (false);
            pitcherControler.gameObject.SetActive(false);
        }

        [PunRPC]
        public void BeforePitch()
        {
            StepUpdate = 0;
            PitchPath = -1;
            Power = 0;
            StepIndex = Step.BeforePitch;
            HitResult.gameObject.SetActive(false);
            distanceTravelled = 0;
            IsSwinged = false;
            pathCreator.gameObject.transform.position = OrigenPos;
            Ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
            Ball.gameObject.SetActive(false);

            BallThrowedAim.gameObject.SetActive(false);
            HomeRunHitParticel.gameObject.SetActive(false);
            NormalHitParticel.gameObject.SetActive(false);
        }


        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity on every frame.
        /// </summary>
        void Update()
        {
            if (PhotonNetwork.LocalPlayer.IsLocal)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    QuitApplication();
                }

                if(StepIndex == Step.StepGap)
                {
                    StepUpdate += Time.deltaTime;
                    if(StepUpdate >= 3)
                    {
                        StepUpdate = 0;
                        photonView.RPC("BeforePitch", RpcTarget.All);
                    }
                }

                if (StepIndex == Step.Pitched)
                {
                    if (!IsThrowed)
                    {
                        StepUpdate += Time.deltaTime;
                        if (StepUpdate >= 0.5f)
                        {
                            StepUpdate = 0;
                            IsThrowed = true;
                            Ball.gameObject.SetActive(true);
                            NinthGames.AudioManager.PlaySound("pitcher_strong");
                            return;
                        }
                    }
                    else
                    {
                        distanceTravelled += CurSpeed * Time.deltaTime;
                        Ball.position = pathCreator.path.GetPointAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
                        Ball.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, EndOfPathInstruction.Stop);
                        if (distanceTravelled / pathCreator.path.length >= 0.7f && !BallThrowedAim.gameObject.activeInHierarchy)
                        {
                            BallThrowedAim.gameObject.SetActive(true);
                        }
                        if (distanceTravelled / pathCreator.path.length >= 1)
                        {
                            
                            if (PhotonNetwork.LocalPlayer.IsMasterClient)
                            {
                                if (BallThrowedAim.anchoredPosition.y > -60 || BallThrowedAim.anchoredPosition.y < -364 || BallThrowedAim.anchoredPosition.x < -150 || BallThrowedAim.anchoredPosition.x > 150)
                                {
                                    BadBall++;
                                    if (BadBall >= 4)
                                    {
                                        if (IsMasterTeam)
                                        {
                                            MasterScore += 1;
                                        }
                                        else
                                        {
                                            CustomScore += 1;
                                        }
                                        Strike = 0;
                                        BadBall = 0;
                                    }
                                    photonView.RPC("ShowResult", RpcTarget.All, "Ball", Strike, BadBall, Out, MasterScore, CustomScore);
                                }
                                else
                                {
                                    Strike++;
                                    if (Strike >= 3)
                                    {
                                        Out += 1;
                                        Strike = 0;
                                        BadBall = 0;
                                    }
                                    photonView.RPC("ShowResult", RpcTarget.All, "Strike", Strike, BadBall, Out, MasterScore, CustomScore);

                                }
                                photonView.RPC("CatchBall", RpcTarget.All, null);
                            }
                            //沒打中
                        }
                        Vector2 ViewportPosition = MainCamera.WorldToViewportPoint(Ball.position);
                        Vector2 WorldObject_ScreenPosition = new Vector2(((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)), ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));
                        BallThrowedAim.anchoredPosition = WorldObject_ScreenPosition;
                    }
                }

                if (IsPitcher && StepIndex == Step.BeforePitch)
                {
                    if (!Input.anyKey && PitchPath != -1 && Power != 0)
                    {
                        ComfirmBall();
                    }

                    if (Input.GetKey(KeyCode.Keypad1) || Input.GetKey(KeyCode.Keypad2) || Input.GetKey(KeyCode.Keypad3))
                    {
                        Power += Time.deltaTime;
                        PowerSlider.gameObject.SetActive(true);
                        PowerSlider.value = Power;
                        if (PitchPath == -1)
                        {
                            if (Input.GetKeyDown(KeyCode.Keypad1))
                            {
                                PitchPath = 0;
                            }
                            else if (Input.GetKeyDown(KeyCode.Keypad2))
                            {
                                PitchPath = 1;
                            }
                            else if (Input.GetKeyDown(KeyCode.Keypad3))
                            {
                                PitchPath = 2;
                            }
                        }
                    }
                    else
                    {
                        if (Input.anyKey)
                        {
                            if (Input.GetKey(KeyCode.W))
                            {
                                if (Target.anchoredPosition.y <= 250)
                                {
                                    Target.anchoredPosition += new Vector2(0, Time.deltaTime * senstive);
                                }
                            }
                            else if (Input.GetKey(KeyCode.S))
                            {
                                if (Target.anchoredPosition.y >= -250)
                                {
                                    Target.anchoredPosition -= new Vector2(0, Time.deltaTime * senstive);
                                }
                            }
                            if (Input.GetKey(KeyCode.A))
                            {
                                if (Target.anchoredPosition.x >= -250)
                                {
                                    Target.anchoredPosition -= new Vector2(Time.deltaTime * senstive, 0);
                                }
                            }
                            else if (Input.GetKey(KeyCode.D))
                            {
                                if (Target.anchoredPosition.x <= 250)
                                {
                                    Target.anchoredPosition += new Vector2(Time.deltaTime * senstive, 0);
                                }
                            }
                        }
                        else if (Target.anchoredPosition != Vector2.zero)
                        {
                            Target.anchoredPosition = new Vector2(Mathf.Lerp(Target.anchoredPosition.x, 0, Time.deltaTime * 10), Mathf.Lerp(Target.anchoredPosition.y, 0, Time.deltaTime * 10));
                        }
                    }
                }
                else if (!IsPitcher && StepIndex == Step.Pitched)
                {
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        photonView.RPC("Swing", RpcTarget.All, null);
                    }
                }
            }
        }


        public void ComfirmBall()
        {
            float CurSpeed = 0;
            Vector2 offset = Target.anchoredPosition - Vector2.zero;
            Vector3 TargetPos = OrigenPos + new Vector3(-offset.x * XRate, offset.y * YRate, 0);
            //if (Power < 0.77f || Power > 0.88f)
            //{
            //    TargetPos += new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), 0) * 0.2f;
            //}
            PowerSlider.gameObject.SetActive(false);
            StepIndex = Step.Pitched;
            photonView.RPC("PitcherThrow", RpcTarget.All, PitchPath, Power, CurSpeed, TargetPos);
        }



        #region Photon Callbacks

        /// <summary>
        /// Called when a Photon Player got disconnected. We need to load a smaller scene.
        /// </summary>
        /// <param name="other">Other.</param>
        public override void OnPlayerLeftRoom(Photon.Realtime.Player other)
        {
            PhotonNetwork.LeaveRoom();
        }

        /// <summary>
        /// Called when the local player left the room. We need to load the launcher scene.
        /// </summary>
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene("Start");
        }

        #endregion

        #region Public Methods

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        public void QuitApplication()
        {
            Application.Quit();
        }

        #endregion

    }
}

