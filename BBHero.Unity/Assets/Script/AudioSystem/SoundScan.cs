﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NinthGames
{
    public class SoundScan : MonoBehaviour
    {
        void Start()
        {
            this.InvokeRepeating("scan", 0, 2f);
        }

        void scan()
        {
            AudioManager.StopAllAudio();//調用隱藏AudioSource組件接口
        }
    }
}
